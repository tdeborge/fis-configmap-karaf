TUTORIAL (Fuse Integration Services)
====================================

	How to use ConfigMaps and environment variables in a FIS-Karaf project.
	(from scratch, using a text editor, all based on FIS 2.0 and CDK 2.3)


	Summary:
	--------
		1) Create FIS-Karaf project
		2) Use ConfigMaps
		3) Use environment variables
		4) Test locally
		5) Deploy and test in OpenShift


	And learn interesting bits like:
	--------------------------------
		- run Karaf locally before deploying in OpenShift 
		- use handy openshift client commands


	Prerequisites:
	--------------
		> Red Hat CDK 2.3 installed and running
		> OpenShift Client installed
		> Environment variables pointing to CDK set
		> Access to Red Hat Maven repositories
		> FIS 2.0 images/templates installed 


	Reference documentation:
	------------------------

		https://access.redhat.com/documentation/en-us/red_hat_jboss_middleware_for_openshift/3/html-single/red_hat_jboss_fuse_integration_services_2.0_for_openshift/



==============================


1) Create Maven project:

	(run command with no tabs/spaces on the left)

	mvn org.apache.maven.plugins:maven-archetype-plugin:2.4:generate \
	-DarchetypeCatalog=https://maven.repository.redhat.com/ga/io/fabric8/archetypes/archetypes-catalog/2.2.195.redhat-000004/archetypes-catalog-2.2.195.redhat-000004-archetype-catalog.xml \
	-DarchetypeGroupId=org.jboss.fuse.fis.archetypes \
	-DarchetypeArtifactId=karaf2-camel-log-archetype \
	-DarchetypeVersion=2.2.195.redhat-000004


2) Create a test property in a ConfigMap 
		
	create
		src/main/fabric8/myconfig.yaml

	with
		kind: ConfigMap
		apiVersion: v1
		metadata:
		  name: myconfig
		  labels:
		    karaf.pid: test.myprops
		data:
		  test.prop.karaf: value from ConfigMap


3) Define an OpenShift environment variable

	We include a test environment variable in the OpenShift's deployment configuration.

		update
			src/main/fabric8/deployment.yml

		include
			TEST_ENV_VAR

		should look like
			...
		    containers:
	        - 
	          resources:
	          	...
	          env:
	          - name: TEST_ENV_VAR
	            value: "OpenShift environment value"


4) Update the Bluepring XML file

	The following code changes show how to handle env variables and properties both in the Blueprint region and Camel region.

		update
			src/main/resources/OSGI-INF/blueprint/camel-log.xml

		include namespaces
			xmlns:cm="http://aries.apache.org/blueprint/xmlns/blueprint-cm/v1.1.0"
			xmlns:ext="http://aries.apache.org/blueprint/xmlns/blueprint-ext/v1.2.0"

		include placeholders
			<cm:property-placeholder persistent-id="test.myprops" id="test.myprops" update-strategy="reload"/>
			<ext:property-placeholder evaluator="fabric8" placeholder-prefix="$[[" placeholder-suffix="]]"/>

		include bean
			<bean id="testprop" class="java.lang.String">
				<argument value="${test.prop.karaf}"/>
			</bean>

		include bean
			<bean id="testenv" class="java.lang.String">
				<argument value="$[[env:TEST_ENV_VAR]]"/>
			</bean>

		include (in Camel):
			<log message="simple expr prop: {{test.prop.karaf}}"/>
			<log message="simple expr env:  ${sysenv.TEST_ENV_VAR}"/>
			<log message="bprint bean prop: ${bean:testprop.toString}"/>
			<log message="bprint bean env:  ${bean:testenv.toString}"/>

	Note CM:
		> cm property-placeholder allows you to load custom properties.
		> cm persistent-id needs to match the ConfigMap's label 'karaf.pid'
		> cm update-strategy needs to be reload. Once Fuse starts, it can then invoke Kubernetes and update the properties.
		> ext property-placeholder allows you to fetch environment variables


5) Include Karaf-cm Feature in POM file

	update
		pom.xml

	include feature
		...
		<feature>fabric8-karaf-cm</feature>
		...


6) Test locally

	To run locally we need to define a local environment variable.

		define:
			export TEST_ENV_VAR='local env value'


	About the ConfigMap property, Karaf needs to be able to resolve it.
	The ConfigMap is obviously not available as we haven't deployed the project in OpenShift yet.

	There are 2 options to move forward:
		a) include a local properties file to simulate the ConfigMap.
		b) deploy the ConfigMap in OpenShift, Karaf will then be able to resolve via the Kubernetes API.


	a) Use of local properties file:

		Using local property files allows you to work without the dependency of an OpenShift environment.
		It simulates the engine is provided with ConfigMap properties, but obviously not using real ConfigMaps. 

	a.1)
		create
			src/main/resources/assembly/etc/test.myprops.cfg

		with
			test.prop.karaf = local prop value

		Note
			> the name of the file needs to match the 'persistence-id' in the Blueprint definition.
			> the property name matches the one defined in the ConfigMap.

	a.2)
		Let's build and run locally

		command:
			mvn clean install -Ddocker.skip.build=true

		The above command builds the project and prepares a runnable Karaf instance.
		The flag included skips the docker build which would attempt to connect to OpenShift.
		For now, we don't want to interact with OpenShift, but work only locally instead.

		command to start karaf:
			target/assembly/bin/standalone

		The output should be obtained by running:
			Fabric8:karaf@root> display

		You should see:
			2017-04-27 16:48:15,207 | INFO  | ... | simple expr prop: local prop value
			2017-04-27 16:48:15,208 | INFO  | ... | simple expr env:  local env value
			2017-04-27 16:48:15,232 | INFO  | ... | bprint bean prop: local prop value
			2017-04-27 16:48:15,232 | INFO  | ... | bprint bean env:  local env value

		You can observe the property value was picked up from the properties file and the env variable resolved.


	b) Deploy the ConfigMap and use locally:

		The second option is to deploy the ConfigMap configuration in OpenShift.
		When running locally, the Karaf instance will query OpenShift and obtain the values.

	b.1) 
		Let's install the ConfigMap:

		create OS project:
			oc new-project test1
 
		deploy ConfigMap:
			oc create -f src/main/fabric8/myconfig.yaml

	b.2) 
		Let's restart Karaf assuming the maven project is already built (from a.2).

		command to start karaf:
			target/assembly/bin/standalone

		The output should be obtained by running:
			Fabric8:karaf@root> display

		You should see:
			2017-04-27 16:51:32,741 | INFO  | ... | simple expr prop: value from ConfigMap
			2017-04-27 16:51:32,741 | INFO  | ... | simple expr env:  local env value
			2017-04-27 16:51:32,770 | INFO  | ... | bprint bean prop: value from ConfigMap
			2017-04-27 16:51:32,771 | INFO  | ... | bprint bean env:  local env value

		You can observe this time the property value corresponds to the ConfigMap definition.


7) Deploy in Openshift

	7.1) Create an OpenShift project

		If a project hasn't been created yet, then run:

		command:
			oc new-project test1


	7.1) Enable Karaf to invoke Kubernetes API

		Karaf obtains ConfigMap data via the Kubernetes API.
		For Karaf to be allowed to invoke the Kubernetes API, access needs to be granted.
		The following command grants 'view' access:

			oc policy add-role-to-user view --serviceaccount=default

		If the above permission is not granted, your pod may throw a message similar to the following:

			"Forbidden!Configured service account doesn't have access. Service account may have been revoked"


	7.2) Running in OpenShift

		Now, let's deploy in OpenShift and observe what the output is:

			command:
		 		mvn clean fabric8:deploy

			you can view the logs with:
				oc get pods
				oc logs <pod_name>

		 	output:
				2017-04-26 15:47:59,088 | INFO  | ... | simple expr prop: value from ConfigMap
				2017-04-26 15:47:59,088 | INFO  | ... | simple expr env:  OpenShift environment value
				2017-04-26 15:47:59,089 | INFO  | ... | bprint bean prop: value from ConfigMap
				2017-04-26 15:47:59,090 | INFO  | ... | bprint bean env:  OpenShift environment value


		Updates in the ConfigMap in OpenShift are picked up automatically by Karaf:

			from the web console, navigate to Resources -> Other Resources.
			Choose 'Config Map' from the drop down list, and then Actions -> Edit YAML.

			update with:
				test.prop.karaf: 'value from ConfigMap updated'

			you can view the logs with:
				oc get pods
				oc logs <pod_name>

			Karaf's output should show:
				2017-04-26 15:49:42,122 | INFO  | ... | simple expr prop: value from ConfigMap updated
				2017-04-26 15:49:42,123 | INFO  | ... | simple expr env:  OpenShift environment value
				2017-04-26 15:49:42,123 | INFO  | ... | bprint bean prop: value from ConfigMap updated
				2017-04-26 15:49:42,123 | INFO  | ... | bprint bean env:  OpenShift environment value


		When updating the environment variable, OpenShift kicks off a new pod with the new env value:

			From the web console, navigate to Applications -> Deployments.
			Select the deployment instance from the entry list and select the tab 'Environment'

				update
					TEST_ENV_VAR

				with
					OpenShift environment updated

			Openshift recognises the change, spins a new pod and discards the previous one showing:
				2017-04-26 15:54:31,137 | INFO  | ... | simple expr prop: value from ConfigMap updated
				2017-04-26 15:54:31,137 | INFO  | ... | simple expr env:  OpenShift environment updated
				2017-04-26 15:54:31,138 | INFO  | ... | bprint bean prop: value from ConfigMap updated
				2017-04-26 15:54:31,138 | INFO  | ... | bprint bean env:  OpenShift environment updated

			you can view the logs with:
				oc get pods
				oc logs <pod_name>
